﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InitechApp
{
    public partial class InitechApp : Form
    {
        public InitechApp()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("WARNING: The consultants are coming.");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("We're putting the cover sheet on ALL TPS reports from now on.  If you could just go ahead and make sure you do that from now on that'd be great.");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            int rInt = r.Next(0, 2); //for ints

            if (rInt == 0)
            {
                MessageBox.Show("PC LOAD LETTER");
            }
            else
            {
                MessageBox.Show("PAPER JAM");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Checking for amount $"+textBox1.Text);
        }
    }
}
