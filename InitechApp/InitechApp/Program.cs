﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Text;

namespace InitechApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            MessageBox.Show("Thank you for starting the CDEF Software!\n Booting up now...");
            Thread.Sleep(1000);
            if (MessageBox.Show("Continue?", "Done Booting!", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new InitechApp());
            }
            else
            {
                MessageBox.Show("EXITING...");
            }
        }

        public class HexString
        {
            private byte[] _data;

            public HexString(string data)
            {
                if ((data.Length & 1) != 0) throw new ArgumentException("Hex string must have an even number of digits.");

                _data = Enumerable.Range(0, data.Length)
                    .Where(x => x % 2 == 0)
                    .Select(x => Convert.ToByte(data.Substring(x, 2), 16))
                    .ToArray();
            }

            public HexString(byte[] data)
            {
                _data = data;
            }


            public override string ToString()
            {
                string hex = BitConverter.ToString(_data);
                return hex.Replace("-", "");
            }

            static public HexString operator ^(HexString LHS, HexString RHS)
            {
                return new HexString
                    (
                        LHS._data.Zip
                            (
                                RHS._data,
                                (a, b) => (byte)(a ^ b)
                            )
                        .ToArray()
                    );
            }
        }

        public static string Close(string key, string input)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
                sb.Append((char)(input[i] ^ key[(i % key.Length)]));
            String result = sb.ToString();

            return result;
        }

    }
}
